#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline author:t
#+OPTIONS: broken-links:nil c:nil creator:nil d:(not "LOGBOOK") date:t e:t
#+OPTIONS: email:nil f:t inline:t num:t p:nil pri:nil prop:nil stat:t tags:t
#+OPTIONS: tasks:t tex:t timestamp:nil title:t toc:nil todo:t |:t
#+Title: GNU Emacs for All
#+Author: Sachin Patil (psachin)
#+Email: psachin@redhat.com
#+Date: EmacsConf 2019
#+Description: https://emacsconf.org/2019/
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.7)

#+OPTIONS: H:2
#+LATEX_CLASS: beamer
#+LATEX_HEADER: \institute{Red Hat}
#+BEAMER_HEADER: \usecolortheme[RGB={0,104,139}]{structure}%deepskyblue
#+BEAMER_THEME: Madrid
#+BEAMER_COLOR_THEME:
#+BEAMER_FONT_THEME: serif
#+BEAMER_INNER_THEME:

#+LaTeX_HEADER: \usepackage{minted}
#+LaTeX_HEADER: \usemintedstyle{emacs}


* IRC
** ERC config: User configuration
   #+BEGIN_SRC emacs-lisp
     (setq erc-user-full-name user-full-name
	   erc-nick user-login-name
	   erc-keywords (quote("training" "swift" "emacs"))
	   erc-current-nick-highlight-type 'all
	   erc-notify-list erc-pals
	   erc-autojoin-enable t
	   erc-autojoin-channels-alist
   #+END_SRC

** ERC config: Channels
   #+BEGIN_SRC emacs-lisp
     ...
     erc-autojoin-enable t
     erc-autojoin-channels-alist
     (quote
      (("oftc" "#tor" "#kernelnewbies" "#kernel-outreachy")
       ("freenode.net" "#emacs" "#emacs-beginners" "#linuxjournal")
       ("GIMPNet" "#outreachy")))
   #+END_SRC

** ERC config: Modules
   #+BEGIN_SRC emacs-lisp
     erc-modules
     (quote
      (autojoin smiley notify))
   #+END_SRC

** ERC config: Logging
   #+BEGIN_SRC emacs-lisp
     ;; Logging
     erc-enable-logging t
     erc-log-mode t
     erc-log-channels-directory "~/.erc/logs/"
     ;; Timestamp position
     erc-insert-timestamp-function 'erc-insert-timestamp-left

     ;; Show channel name in the notification.
     ;; Please apply: https://gitlab.com/snippets/1790218
     erc-notifications-show-channel t)
   #+END_SRC

** ERC config: Extra config
   #+BEGIN_SRC emacs-lisp
     ;; Turn off linum for ERC buffer
     (defun psachin/turn-off-linum ()
       "Turn off linum for ERC buffers."
       (interactive)
       (display-line-numbers-mode 0))

     (add-hook 'erc-mode-hook 'psachin/turn-off-linum)
   #+END_SRC

** ERC config: Start server
   #+BEGIN_SRC emacs-lisp
     (defun erc-start()
       "Start ERC."
       (interactive)
       (erc :server "irc.freenode.net")
       (erc :server "irc.oftc.net")
       (erc :server "irc.gnome.org"))
   #+END_SRC

   - https://gitlab.com/psachin/emacs.d/blob/master/irc.org

* Documents
** Documents: LaTeX

   #+CAPTION: LaTeX Document
   #+attr_latex: :width 8cm
   [[./assets/latex_doc.png]]

** Documents: Image

   #+CAPTION: Logo
   #+ATTR_LATEX: :width 3cm
   [[./assets/gnome-x-office-presentation.png]]

** Documents: Table

   #+CAPTION: Swift Partition table
   #+ATTR_LAtex: :align |c|c|c|c|
   |               | Replica # 3 | Replica # 2 | Replica # 11 |
   |---------------+-------------+-------------+--------------|
   | Partition # 1 | D #2        | D #3        | D #4         |
   | Partition # 2 | D #3        | D #4        | D #5         |
   | Partition # 3 | D #4        | D #5        | D #6         |
   | Partition # 4 | D #5        | D #6        | D #7         |
   | Partition # 5 | D #6        | D #7        | D #8         |
   | Partition # 6 | D #7        | D #8        | D #1         |
   | Partition # 7 | D #8        | D #1        | D #2         |
   | Partition # 8 | D #1        | D #2        | D #3         |

** Documents: Source code

   #+BEGIN_SRC bash
     ,#+BEGIN_SRC python -n
       import os

       def foo(os):
	   pass
     ,#+END_SRC
   #+END_SRC

** Documents: Flow diagram(=ditaa=)

   #+BEGIN_SRC bash
     .           +--------+   +-------+    +-------+
     .           |        | --+ ditaa +--> |       |
     .           |  Text  |   +-------+    |diagram|
     .           |Document|   |!magic!|    |       |
     .           |     {d}|   | cFF3  |    | cGRE  |
     .           +---+----+   +-------+    +-------+
     .                :                         ^
     .                |       Lots of work      |
     .                +-------------------------+
   #+END_SRC

** Documents: Flow diagram(=ditaa=)

   #+CAPTION: Image imported using ditaa
   #+ATTR_LATEX: :width 8cm
   [[./image.png]]

* Trello
** Trello

   #+ATTR_LATEX: :width 6cm :height
   [[./assets/trello-logo-blue.png]]

** org-trello
   http://org-trello.github.io/trello-setup.html

   - Example:
     #+BEGIN_SRC emacs-lisp
       ;; ~/.emacs.d/.trello/USERNAME.el
       (setq org-trello-consumer-key "abc..."
             org-trello-access-token "def...")
     #+END_SRC

** Connect to the Trello board
   #+BEGIN_SRC emacs-lisp
     (use-package org-trello
       :ensure t
       :config
       (custom-set-variables '(org-trello-files
			       '("~/trello/myboard.org"))))
   #+END_SRC

* Presentation
** Presentation: LaTeX Beamer
   #+CAPTION: Beamer Presentation
   #+attr_latex: :width 7cm
   [[./assets/presentation.png]]

   https://opensource.com/article/18/4/how-create-latex-documents-emacs

** Presentation: Reveal.js

   - https://github.com/yjwen/org-reveal
   - https://opensource.com/article/18/2/how-create-slides-emacs-org-mode-and-revealjs

** Presentation: org-tree-slide

   - https://github.com/takaxp/org-tree-slide

* Version control
** Version control: magit
   - https://magit.vc/
   - https://opensource.com/article/19/1/how-use-magit
** Version control: git-timemachine
   - https://gitlab.com/pidu/git-timemachine

* Agenda
** Agenda: org-agenda
   #+BEGIN_SRC emacs-lisp
     (setq org-agenda-files '("~/agenda/work/work.org"
			      "~/agenda/todo/read.org"
			      "~/agenda/todo/todo.org"))
   #+END_SRC

** Agenda: org-agenda
   #+BEGIN_SRC emacs-lisp
     (setq org-capture-templates
     '(("t" "Todo" entry
	      (file+headline "~/agenda/todo/todo.org" "Tasks")
	      "* TODO %i%?\n %U\n %a")
	     ("n" "Notes" entry
	      (file+headline "~/agenda/notes.org" "Notes")
	      "* %A%?\n %U")
	     ("i" "Insights" entry
	      (file+headline "~/agenda/work/work.org" "Insights")
	      "* %u%?")))
   #+END_SRC


** Agenda: org-agenda
   - https://gitlab.com/psachin/emacs.d/blob/master/mytime.org
   - https://emacs.cafe/emacs/orgmode/gtd/2017/06/30/orgmode-gtd.html

* Diary
** The Diary file
   #+BEGIN_SRC bash
     Friday
        20:10 to 20:30 Meeting with Rey
	21:30 to 24:30 FSF IRC meeting
   #+END_SRC

** Desktop Notification
   #+ATTR_HTML: :width 100% :height
   [[./assets/notification.png]]

   https://psachin.gitlab.io/emacs_diary_desktop_notification.html

* System
** System
   - Man pages: =M-x man=
   - Terminal: =M-x eshell=
   - File browser: Dired mode
   - grep: =M-x grep=

* Blogging
** Blogging: org-jekyll

   - https://orgmode.org/worg/org-tutorials/org-jekyll.html
** org-publish
   #+BEGIN_SRC emacs-lisp
     (require 'ox-publish)
     (setq org-publish-project-alist
	   '(

	     ;; ... add all the components here (see below)...

	     ))
   #+END_SRC
   - https://orgmode.org/worg/org-tutorials/org-publish-html-tutorial.html
   - https://gitlab.com/psachin/psachin.gitlab.io/blob/master/publish.el

* Games
** Games
   - =M-x snake=
   - =M-x tetris=
   - =M-x doctor=
   - ...

** Programming?
   - Support to all major programming languages
   - Syntax coloring etc.
* Thank You
** Thank You

   \center{Questions?}

   \vskip 5ex

   https://psachin.gitlab.io

   [[mailto:psachin@redhat.com][psachin@redhat.com]]

   Made with Love, [[https://www.gnu.org/software/emacs/][GNU Emacs]] & [[https://orgmode.org/][Orgmode]]
